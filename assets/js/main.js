var matchpc = false;
var matchmb = false;

$(document).ready(function () {

  if($(window).width() > 768) {
    $(window).enllax();
  }
    // $(window).resize(function(){
      
    //   location.reload();
    // });


  if($(window).width() == 768) {
    $(window).resize(function(){
      
      location.reload();
    });
  }

  $('a').each(function () {
    $(this).click(function (e) {
      if ($(this).attr('href') == '#') {
        e.preventDefault();
      }
    });
  });

  $('.click-more-ourwork').click(function(e){
    e.preventDefault();
    $('.more-ourwork').slideToggle();
  });

  $('.download-blog').click(function(e){
    console.log(44);
    e.preventDefault();
    $('.blog-thank').addClass('active');
    $('.overlay-container.blog').addClass('active');
  });

  $('.close-thank').click(function(e){
     e.preventDefault();
    $('.blog-thank').removeClass('active');
    $('.overlay-container.blog').removeClass('active');
  });

  $('.scroll-down').click(function(e){
     e.preventDefault();
    $('html, body').animate({ 
        scrollTop: $($(this).attr('href')).offset().top
    }, 1000, 'linear');

  });

  $('.search-menu').click(function(e){
     e.preventDefault();
    $('.all-menu').removeClass('active');
    $('.search-bar').addClass('active');
  });

  $('.search-bar .close').click(function(e){
     e.preventDefault();
     $('.search-bar').removeClass('active');
      $('.all-menu').addClass('active');
  });

  $('.close-menu').click(function(e){
     e.preventDefault();
      $('.collapse').removeClass('show');
      $('button.navbar-toggler').attr('aria-expanded','false');
  });

  $('.navbar-toggler').click(function(e){
     e.preventDefault();
      $('.collapse').addClass('show');
      $('button.navbar-toggler').attr('aria-expanded','true');
  });


   $('.choose-cate').owlCarousel({
      center: true,
      items: 1,
      autoplayTimeout: 4000,
        responsive: {
        0: {
          items: 1
        },
        400: {
          items: 2
        },
        768: {
          items: 3
        },
        998: {
          items: 5,
          center: false
        }
      }
    });


   // back to top
  var myVar;
  //hide or show the "back to top" link
  $(window).scroll(function(){
    if($(this).scrollTop() > 300){
    clearTimeout(myVar);
    $('.backtotop').addClass('cd-is-visible');
    
    myVar = setTimeout(function(){ $('.backtotop').removeClass('cd-is-visible cd-fade-out'); }, 2000);
    } else {
    $('.backtotop').removeClass('cd-is-visible cd-fade-out');
    }
  });
  
  $('.backtotop').click(function(event){
    event.preventDefault();
    $('body,html').animate({
        scrollTop: 0,
      },700);
  });

  $('.focus-text').focus(function(){
  $(this).closest('div').addClass('focus-t');
  })
  $('.focus-text').blur(function(){
    if($('.focus-text').length > 0 && $('.focus-text').val() != ''){
    $(this).closest('div').addClass('focus-t');    
    }
    else{
    $(this).closest('div').removeClass('focus-t');  
    }

  })

 
});
